#!/usr/bin/perl

require Text::CSV;
use Data::Dumper;

my $NUM_BOARDS = 40;
my $LANE_LENGTH = 60;
my $NUM_INCHES_PER_FOOT = 12;
my $LANE_LENGTH_INCHES = $LANE_LENGTH*$NUM_INCHES_PER_FOOT;

# Read in the file into a hash reference that we can work with
my $forward_filename = "../patterns/2014RegionalShark/kegel/forward.csv"; #$ARGV[0];
my $forward = loadFile($forward_filename);

my $reverse_filename = "../patterns/2014RegionalShark/kegel/reverse.csv"; #$ARGV[1];
my $reverse = loadFile($reverse_filename);

my $output_file = "../patterns/2014RegionalShark/blocks.csv";

open my $out, ">", $output_file or die "Failed to open ".$output_file;

my @output_array = ();
my $last_oiled;
for (@$forward)
{
	my $val = $_;

	my $start = int($val->{startl}*$NUM_INCHES_PER_FOOT);
	my $end = int($val->{endl}*$NUM_INCHES_PER_FOOT);
	my $length = $val->{feet}*$NUM_INCHES_PER_FOOT;

	my $start_board = $val->{startb};
	$start_board =~ s/L//g;

	my $end_board = $val->{stopb};
	$end_board =~ s/R//g;
	$end_board = $NUM_BOARDS-$end_board;
	
	my $width = $end_board - $start_board;
	my $area = $length * $width;

	my $volume = $val->{toil};

	my $vol_per_bit = $volume/$area;

	print $volume." ".$area." ".$vol_per_bit."\n";
	$last_oiled = $end;
	for(my $i = $start; $i < $end; $i++)
	{
		for(my $j = 0; $j < $start_board; $j++)
		{
			$output_array[$i][$j] += 0;
		}
		for(my $j = $start_board; $j < $end_board; $j++)
		{
			$output_array[$i][$j] += $vol_per_bit;
		}
		for(my $j = $end_board; $j < $NUM_BOARDS; $j++)
		{
			$output_array[$i][$j] += 0;
		}
	}
}
for(my $i = $last_oiled; $i<$LANE_LENGTH_INCHES; $i++)
{
	for(my $j = 0; $j<$NUM_BOARDS; $j++)
	{
		$output_array[$i][$j] = 0;
	}
}

for (@output_array)
{
	for (@$_)
	{
		print $out $_.",";
	}
	print $out "\n";
}

sub loadFile
{
	my $csv = Text::CSV->new();
	my $filename = shift;
	open my $io, "<", $filename or die "Failed to open the file";
	$csv->column_names ($csv->getline ($io));
	my $data = $csv->getline_hr_all($io);
	close $io;	
	return $data;
}