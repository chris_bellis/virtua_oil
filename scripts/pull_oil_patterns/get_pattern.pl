#!/usr/bin/perl

use strict;
use warnings;

use LWP::Simple;

for my $i(0...999)
{
	print "Getting page ".$i."...";
	my $content = get("http://www.kegel.net/V3/PatternLibraryPatternLoadData.aspx?ID=$i") or die 'Unable to get page';
	if(index($content, "Forward Oil Table Start") != -1)
	{
		print "VALID\n";
	}
	else
	{
		print "INVALID\n";
	}
}
exit 0;

